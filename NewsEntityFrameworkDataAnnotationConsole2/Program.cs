﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using NewsEntityFrameworkDataAnnotationConsole2.Models;

namespace NewsEntityFrameworkDataAnnotationConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            NewsContext newsContext = new NewsContext();
            newsContext.Languages.Add(new Language()
            {
                Name = "Japanese"
            });
            newsContext.SaveChanges();
        }
    }
}
