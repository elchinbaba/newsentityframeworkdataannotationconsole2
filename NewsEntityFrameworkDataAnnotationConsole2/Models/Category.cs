﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsEntityFrameworkDataAnnotationConsole2.Models
{
    class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
