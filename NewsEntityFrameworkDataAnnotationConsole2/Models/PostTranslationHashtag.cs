﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsEntityFrameworkDataAnnotationConsole2.Models
{
    class PostTranslationHashtag
    {
        [Key]
        public int Id { get; set; }
        public int HashtagId { get; set; }
        [ForeignKey("HashtagId")]
        public Hashtag Hashtag { get; set; }
        public int PostTranslationId { get; set; }
        [ForeignKey("PostTranslationId")]
        public PostTranslation PostTranslation { get; set; }
    }
}
