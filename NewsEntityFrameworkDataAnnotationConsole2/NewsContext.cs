﻿//using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using NewsEntityFrameworkDataAnnotationConsole2.Models;
using System.Data.Entity;


namespace NewsEntityFrameworkDataAnnotationConsole2
{
    class NewsContext : DbContext
    {
        public NewsContext() : base() {}
        //public NewsContext(DbContextOptions<NewsContext> options) : base(options) { }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryTranslation> CategoryTranslations { get; set; }
        public DbSet<Hashtag> Hashtags { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostTranslation> PostTranslations { get; set; }
        public DbSet<PostTranslationHashtag> PostTranslationHashtags { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=WIN-D6GRQOTSRKP\SQLEXPRESS;Database=test;Trusted_Connection=True");
        //}
    }
}
